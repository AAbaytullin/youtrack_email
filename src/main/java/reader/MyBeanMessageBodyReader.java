package reader;

import entity.Item;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.stream.Collectors;

public class MyBeanMessageBodyReader
        implements MessageBodyReader<Item> {

    @Override
    public boolean isReadable(Class<?> type, Type genericType,
                              Annotation[] annotations, MediaType mediaType) {
        return type == Item.class;
    }

    @Override
    public Item readFrom(Class<Item> type,
                         Type genericType,
                         Annotation[] annotations, MediaType mediaType,
                         MultivaluedMap<String, String> httpHeaders,
                         InputStream entityStream)
            throws IOException, WebApplicationException {
        Item item = new Item();
        try {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(entityStream))) {
                String result = br.lines().collect(Collectors.joining("\n"));
                item.setJson(result);
                if (result.contains("error")) {
                    getError(result, item);
                }

                if (result.contains("id")) {
                    getValue(result, item);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return item;
    }

    private void getError(String json, Item item) {
        try {
            JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(json);

            String error = (String) responseJsonObject.get("error");
            String error_description = (String) responseJsonObject.get("error_description");
            item.setError(error);
            item.setError_description(error_description);
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
    }

    private void getValue(String json, Item item) {
        try {
            JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(json);

            String id = (String) responseJsonObject.get("id");
            String type = (String) responseJsonObject.get("type");
            item.setId(id);
            item.setType(type);
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
    }

}
