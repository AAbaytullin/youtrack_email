import com.sun.mail.imap.IMAPMessage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class EmailManager {
    private String user;
    private String pass;
    private String host;
    private String saveDirectory;
    private String youtrackEmail;
    Store store;

    public EmailManager(String user, String pass, String host, String saveDirectory, String youtrackEmail) {
        this.user = user;
        this.pass = pass;
        this.host = host;
        this.saveDirectory = saveDirectory;
        this.youtrackEmail = youtrackEmail;
        // Создание свойств
    }

    public boolean initialize() {
        try {
            Properties props = new Properties();

            //включение debug-режима
            //props.put("mail.debug", "true");

            //Указываем протокол - IMAP с SSL
            props.put("mail.store.protocol", "imaps");
            Session session = Session.getInstance(props);
            store = session.getStore();
        } catch (NoSuchProviderException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public void printEmails(String folder) {
        try {
            //подключаемся к почтовому серверу
            store.connect(host, user, pass);

            //create the folder object and open it
            Folder emailFolder = store.getFolder(folder);
            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
            System.out.println("messages.length---" + messages.length);

            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];
                System.out.println("---------------------------------");
                System.out.println("Email Number " + (i + 1));
                System.out.println("Subject: " + message.getSubject());
                System.out.println("From: " + message.getFrom()[0]);
                System.out.println("Text: " + message.getContent().toString());

            }

            //close the store and folder objects
            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<YTrackIssue> getNewEmails(String folderMain, String folderProcessed) {
        List<YTrackIssue> list = new ArrayList<>();
        try {
            //подключаемся к почтовому серверу
            store.connect(host, user, pass);

            //получаем папку с входящими сообщениями
            Folder inbox = store.getFolder(folderMain);
            Folder inboxYoutrack = store.getFolder(folderProcessed);

            //открываем её только для чтения
            inbox.open(Folder.READ_WRITE);
            inboxYoutrack.open(Folder.READ_WRITE);
            boolean hasMessages = inbox.getMessageCount() > 0;
            System.out.println("Find " + inbox.getMessageCount() + " emails.");
            if (hasMessages) {
                for (Message m : inbox.getMessages()) {
                    //получаем последнее сообщение (самое старое будет под номером 1)
                    //Message m = inbox.getMessage(inbox.getMessageCount());
                    String emailAddress = checkYouTrackMail((IMAPMessage) m, youtrackEmail);
                    if (emailAddress != null) {
                        System.out.println("Find message for youtrack: " + emailAddress);
                        YTrackIssue yTrackIssue = getYTrackIssue(m, emailAddress);
                        list.add(yTrackIssue);
                    }
                }
                inbox.copyMessages(inbox.getMessages(), inboxYoutrack);
                System.out.println("Move all messages to folder: " + folderProcessed + ".");
                for (Message m : inbox.getMessages()) {
                    m.setFlag(Flags.Flag.DELETED, true);
                }
                inbox.expunge();
            }

            inbox.close();
            inboxYoutrack.close();
            store.close();
        } catch (MessagingException e) {
            System.out.println("Error with connect to the mail server");
            System.out.println(e.getMessage());
        }
        return list;
    }

    private String checkYouTrackMail(IMAPMessage m, String youtrackAddress) throws MessagingException {
        Address[] addresses = m.getAllRecipients();
        if (addresses == null) {
            return null;
        }
        for (Address r : addresses) {
            InternetAddress internetAddress = (InternetAddress) r;
            System.out.println(internetAddress.getAddress());
            if (internetAddress.getAddress().contains(youtrackAddress)) {
                return internetAddress.getAddress();
            }
        }
        return null;
    }

    private YTrackIssue getYTrackIssue(Message m, String emailAddress) throws MessagingException {
        int at = emailAddress.indexOf("@");
        int plus = emailAddress.indexOf("+") + 1;
        String projectName = emailAddress.substring(plus, at).trim();
        System.out.println("Project Name: " + projectName);
        String taskName = m.getSubject();
        System.out.println("Task Name: " + m.getSubject());

        System.out.println("From: " + m.getFrom()[0].toString());
        Arrays.stream(m.getAllRecipients()).forEach(System.out::println);
        List<String> attachFiles = new ArrayList<>();
        YTrackIssue yTrackIssue = new YTrackIssue(projectName, taskName, "", attachFiles, null);
        getYTrackIssueFromPart(m, yTrackIssue);
        return yTrackIssue;
    }

    private void getYTrackIssueFromPart(Part part, YTrackIssue yTrackIssue) throws MessagingException {
        System.out.println("ContentType: " + part.getContentType());
        System.out.println("Disposition: " + part.getDisposition());
        if (part.isMimeType("text/*")) {
            try {
                yTrackIssue.setDescription((String) part.getContent());
                System.out.println("Description: " + yTrackIssue.getDescription());
            } catch (IOException e) {
                System.out.println("Error with read text content");
                System.out.println(e.getMessage());
            }
        }

        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
            MimeBodyPart bp = (MimeBodyPart) part;
            String fileName = bp.getFileName();
            String fullFileName = saveDirectory + File.separator + fileName;
            yTrackIssue.getAttachments().add(fullFileName);
            try {
                bp.saveFile(fullFileName);
            } catch (IOException e) {
                System.out.println("Error with read attachments");
                System.out.println(e.getMessage());
            }
        }


        if (part.isMimeType("multipart/*")) {
            Multipart mp = null;
            try {
                mp = (Multipart) part.getContent();
            } catch (IOException e) {
                System.out.println("Error with read multipart");
                System.out.println(e.getMessage());
            }
            for (int i = 0; i < mp.getCount(); i++) {
                getYTrackIssueFromPart(mp.getBodyPart(i), yTrackIssue);
            }
        }
    }

}
