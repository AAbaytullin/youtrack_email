import java.util.List;

public class Main {

    private static EmailManager emailManager;
    private static YouTrackManager youTrackManager;

    public static void main(String[] args) {
        System.out.println("Start");
        //ConfigReader.getInstance();
        final String user = ConfigReader.getPropertyByName("email.user");
        final String pass = ConfigReader.getPropertyByName("email.pass");
        final long timeout = Long.valueOf(ConfigReader.getPropertyByName("email.check.timeout"));
        final String host = ConfigReader.getPropertyByName("server.imap");
        final String saveDirectory = ConfigReader.getPropertyByName("attachment.dir");
        final String folderMain = ConfigReader.getPropertyByName("folder.main");
        final String folderProcessed = ConfigReader.getPropertyByName("folder.processed");
        final String youtrackEmailAddress = ConfigReader.getPropertyByName("email.target");

        final String youTrackPath = ConfigReader.getPropertyByName("youtrack.path");
        final String youTrackToken = ConfigReader.getPropertyByName("youtrack.token");
        final String youTrackProject = ConfigReader.getPropertyByName("youtrack.project");

        System.out.println("user=" + user);
        System.out.println("pass=" + pass);
        System.out.println("host=" + host);
        System.out.println("folderMain=" + folderMain);
        System.out.println("folderProcessed=" + folderProcessed);
        System.out.println("youtrackEmailAddress=" + youtrackEmailAddress);
        youTrackManager = new YouTrackManager(youTrackPath, youTrackToken, youTrackProject);
        //System.out.println(youTrackManager.checkJson());
        //youTrackManager.checkConnection();
        //youTrackManager.attachFile("2-3137");
        //youTrackManager.createIssue(null);
        //System.out.println(youTrackManager.checkJson());

        emailManager = new EmailManager(user, pass, host, saveDirectory, youtrackEmailAddress);
        processEmail(folderMain, folderProcessed, timeout);

        System.out.println("Finish");
    }

    public static void processEmail(String folderMain, String folderProcessed, long timeout) {
        if (!emailManager.initialize()) {
            System.out.println("Error with initialization JavaMail");
            return;
        }
        //emailManager.printEmails(folderMain);

        boolean error = false;

        while (!error) {
            try {
                System.out.println("Start checking email " + System.currentTimeMillis());
                List<YTrackIssue> list = emailManager.getNewEmails(folderMain, folderProcessed);
                for (YTrackIssue y : list) {
                    if (youTrackManager.createIssue(y)) {
                        for (String filename : y.getAttachments()) {
                            error = !youTrackManager.attachFile(y.getIssueId(), filename);
                        }
                    } else {
                        error = true;
                    }
                }
                System.out.println("Finish checking email " + System.currentTimeMillis());
                Thread.sleep(timeout);
            } catch (Exception e) {
                System.out.println("ERROR " + e.getMessage());
                error = true;
            }
        }
    }
}

