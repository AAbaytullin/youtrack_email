import entity.Issue;
import entity.Item;
import entity.Project;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import reader.MyBeanMessageBodyReader;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

public class YouTrackManager {
    private String path;
    private String token;
    private String project;

    public YouTrackManager(String path, String token, String project) {
        this.path = path;
        this.token = token;
        this.project = project;
    }

    public boolean initialize(String path, String token) {
        return false;
    }

    public boolean checkConnection() {

        Client client = ClientBuilder.newBuilder().register(MyBeanMessageBodyReader.class).build();

        WebTarget webTarget = client.target(path);
        WebTarget resourceWebTarget = webTarget.path("admin/projects");
        WebTarget helloworldWebTargetWithQueryParam =
                resourceWebTarget.queryParam("fields", "id,name,shortName,createdBy%28login,name,id%29,leader%28login,name,id%29");

        Invocation.Builder invocationBuilder =
                helloworldWebTargetWithQueryParam.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("Authorization", "Bearer perm:YWJhaXR1bGxpbg==.NTktMQ==.ZiafDOPkSoTiRz8r9aEX5M2rsF1LRZ");
        invocationBuilder.header("Content-Type", "application/json");


        Response response = invocationBuilder.get();
        System.out.println(response.getStatus());
        //Item item = response.readEntity(Item.class);
        String item = response.readEntity(String.class);
        System.out.println(item);
        return false;
    }

    public boolean createIssue(YTrackIssue yTrackIssue) {
        try {

            Client client = ClientBuilder.newBuilder()
                    .register(MyBeanMessageBodyWriter.class)
                    .register(MyBeanMessageBodyReader.class)
                    .build();

            WebTarget webTarget = client.target(path);
            WebTarget resourceWebTarget = webTarget.path("issues");

            Invocation.Builder invocationBuilder =
                    resourceWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("Authorization", "Bearer perm:YWJhaXR1bGxpbg==.NTktMQ==.ZiafDOPkSoTiRz8r9aEX5M2rsF1LRZ");
            invocationBuilder.header("Content-Type", "application/json");

            Issue issue = new Issue();
            issue.setSummary(yTrackIssue.getTaskName());
            issue.setDescription(yTrackIssue.getDescription());
            issue.setProject(new Project(project));

            Response response = invocationBuilder.post(Entity.json(issue));
            System.out.println(response.getStatus());
            Item item = response.readEntity(Item.class);
            System.out.println(item.getJson());
            System.out.println(item);
            yTrackIssue.setIssueId(item.getId());
            if (item.getError() != null) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("(createIssue) Error with issue:");
            System.out.println(yTrackIssue);
            System.out.println(e);
            return false;
        }
        return true;
    }

    public boolean attachFile(String issueId, String fileName) {
        try {
            Client client = ClientBuilder.newBuilder()
                    .register(MultiPartFeature.class)
                    .register(MyBeanMessageBodyReader.class)
                    .build();

            WebTarget webTarget = client.target(path);
            WebTarget issueWebTarget = webTarget.path("issues");
            WebTarget thisIssueWebTarget = issueWebTarget.path(issueId);
            WebTarget attachThisIssueWebTarget = thisIssueWebTarget.path("attachments");

            Invocation.Builder invocationBuilder =
                    attachThisIssueWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("Authorization", "Bearer perm:YWJhaXR1bGxpbg==.NTktMQ==.ZiafDOPkSoTiRz8r9aEX5M2rsF1LRZ");
            //invocationBuilder.header("Content-Type", "multipart/form-data");
            FileDataBodyPart filePart = new FileDataBodyPart("my_file", new File(fileName));
            FormDataMultiPart multipart = (FormDataMultiPart) new FormDataMultiPart()
                    //.field("foo", "bar")
                    .bodyPart(filePart);

            Response response = invocationBuilder.post(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA));
            System.out.println(response.getStatus());
            Item item = response.readEntity(Item.class);
            System.out.println(item.getJson());
            System.out.println(item);
            if (item.getError() != null) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("(attachFile) Error with issue:");
            System.out.println(issueId);
            System.out.println("(attachFile) Error with file:");
            System.out.println(fileName);
            System.out.println(e);
            return false;
        }
        return true;
    }
}
